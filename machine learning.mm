<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="machine learning" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1481132230318"><hook NAME="MapStyle" zoom="1.5">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="week 1" FOLDED="true" POSITION="left" ID="ID_305187539" CREATED="1481731909942" MODIFIED="1481731929022">
<edge COLOR="#0000ff"/>
<node TEXT="unsupervised learning" ID="ID_1516771735" CREATED="1481731946181" MODIFIED="1481731953556">
<node TEXT="Unsupervised learning allows us to approach problems with little or no idea what our results should look like. We can derive structure from data where we don&apos;t necessarily know the effect of the variables." ID="ID_940712842" CREATED="1481733190403" MODIFIED="1481733190403"/>
<node TEXT="We can derive this structure by clustering the data based on relationships among the variables in the data." ID="ID_1504603364" CREATED="1481733190403" MODIFIED="1481733190403"/>
<node TEXT="With unsupervised learning there is no feedback based on the prediction results." ID="ID_1987499237" CREATED="1481733190410" MODIFIED="1481733190410"/>
<node TEXT="Example:" FOLDED="true" ID="ID_88527125" CREATED="1481733202525" MODIFIED="1481733202525">
<node TEXT="Clustering: Take a collection of 1,000,000 different genes, and find a way to automatically group these genes into groups that are somehow similar or related by different variables, such as lifespan, location, roles, and so on." ID="ID_12086217" CREATED="1481733202525" MODIFIED="1481733202525"/>
<node TEXT="Non-clustering: The &quot;Cocktail Party Algorithm&quot;, allows you to find structure in a chaotic environment. (i.e. identifying individual voices and music from a mesh of sounds at a cocktail party)." FOLDED="true" ID="ID_811671689" CREATED="1481733202525" MODIFIED="1481733202525">
<node TEXT="cocktail party algorithm" FOLDED="true" ID="ID_763058400" CREATED="1481731916500" MODIFIED="1481733251302" LINK="https://en.wikipedia.org/wiki/Cocktail_party_effect">
<node TEXT="[W,s,v] = svd( (repmat(sum(x.*x,1),size(x,1),1).*x) * x&apos; );" ID="ID_688869338" CREATED="1481732041885" MODIFIED="1481732105534"/>
</node>
</node>
</node>
</node>
<node TEXT="supervised learning" ID="ID_475008146" CREATED="1482115358727" MODIFIED="1482115367473">
<node TEXT="notation" ID="ID_1154167598" CREATED="1482115370303" MODIFIED="1482115372730">
<node TEXT="m" ID="ID_1402125448" CREATED="1482115373007" MODIFIED="1482115424240">
<node TEXT="number of  training examples" ID="ID_1765863832" CREATED="1482115426628" MODIFIED="1482115426628"/>
</node>
<node TEXT="x" ID="ID_1199956575" CREATED="1482115428135" MODIFIED="1482115430225">
<node TEXT="input variable" ID="ID_1931291465" CREATED="1482115439831" MODIFIED="1482115443331"/>
<node TEXT="features" ID="ID_930399232" CREATED="1482115444503" MODIFIED="1482115446073"/>
</node>
<node TEXT="y" ID="ID_445233975" CREATED="1482115430983" MODIFIED="1482115431537">
<node TEXT="output variable" ID="ID_1989394104" CREATED="1482115452551" MODIFIED="1482115455874"/>
<node TEXT="target variable" ID="ID_814754185" CREATED="1482115458184" MODIFIED="1482115461314"/>
</node>
<node TEXT="\latex $(x,y)$" ID="ID_1493414876" CREATED="1482115533825" MODIFIED="1482116258731">
<node TEXT="single training example" ID="ID_690879983" CREATED="1482115538441" MODIFIED="1482115546755"/>
</node>
<node TEXT="\latex $(x^i, y^i)$" ID="ID_1210549768" CREATED="1482115564541" MODIFIED="1482116244418">
<node TEXT="i-th training example" ID="ID_1010809681" CREATED="1482115578102" MODIFIED="1482115587808"/>
</node>
<node TEXT="h" ID="ID_491429338" CREATED="1482115673563" MODIFIED="1482115707412">
<font BOLD="true" ITALIC="true"/>
<node TEXT="function" ID="ID_1995006626" CREATED="1482115687283" MODIFIED="1482115690125">
<node TEXT="maps x -&gt; y" ID="ID_1816989765" CREATED="1482115691851" MODIFIED="1482115696638"/>
</node>
<node TEXT="hypothesis function" ID="ID_83243276" CREATED="1482115698291" MODIFIED="1482118722555"/>
<node TEXT="not a great name" ID="ID_1183713543" CREATED="1482115708596" MODIFIED="1482115717373"/>
<node TEXT="\latex $h_{\theta} = \theta_{0} + \theta_{1}x$" ID="ID_471254837" CREATED="1482115910695" MODIFIED="1482116214739">
<node TEXT="predicts that y is a linear function" ID="ID_1243633443" CREATED="1482116195379" MODIFIED="1482116203589"/>
<node TEXT="linear regression" ID="ID_12807617" CREATED="1482115780339" MODIFIED="1482117300786">
<font BOLD="false"/>
<node TEXT="with one variable" FOLDED="true" ID="ID_1306523554" CREATED="1482115786884" MODIFIED="1482115790181">
<node TEXT="&quot;univariate&quot;" ID="ID_288964776" CREATED="1482115791748" MODIFIED="1482115796037"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="training set" ID="ID_670590502" CREATED="1482115515576" MODIFIED="1482115518762"/>
<node TEXT="minimize" ID="ID_331586278" CREATED="1482117463683" MODIFIED="1482117741365">
<node TEXT="\latex $ \frac{1}{2m}\sum^{m}_{i=1} ( h_{\theta}(x^{(i)}) - y^{(i)})^2$" ID="ID_866370067" CREATED="1482117716352" MODIFIED="1482118080208"/>
<node TEXT="over parameters" ID="ID_1927343439" CREATED="1482117696231" MODIFIED="1482118634125">
<node TEXT="\latex $\theta_{0}$  $\theta_{1}$" ID="ID_1143849240" CREATED="1482117825899" MODIFIED="1482117857906">
<font BOLD="true"/>
</node>
</node>
</node>
<node TEXT="" ID="ID_1134503150" CREATED="1482118431093" MODIFIED="1482118443281">
<hook URI="overview.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="functions" ID="ID_1270849500" CREATED="1482118690224" MODIFIED="1482118692841">
<node TEXT="hypothesis" ID="ID_775169518" CREATED="1482118693144" MODIFIED="1482118736755">
<font BOLD="true"/>
<node TEXT="h" ID="ID_630354106" CREATED="1482115673563" MODIFIED="1482115707412">
<font BOLD="true" ITALIC="true"/>
<node TEXT="function" ID="ID_1783973725" CREATED="1482115687283" MODIFIED="1482115690125">
<node TEXT="maps x -&gt; y" ID="ID_830021141" CREATED="1482115691851" MODIFIED="1482115696638"/>
</node>
<node TEXT="hypothesis function" ID="ID_1294194612" CREATED="1482115698291" MODIFIED="1482118722555"/>
<node TEXT="not a great name" ID="ID_1085747544" CREATED="1482115708596" MODIFIED="1482115717373"/>
<node TEXT="previously for one feature" ID="ID_668051560" CREATED="1483218569319" MODIFIED="1483218580013">
<node TEXT="\latex $h_{\theta} = \theta_{0} + \theta_{1}x$" FOLDED="true" ID="ID_1411742781" CREATED="1482115910695" MODIFIED="1482116214739">
<node TEXT="predicts that y is a linear function" ID="ID_1211664236" CREATED="1482116195379" MODIFIED="1482116203589"/>
<node TEXT="linear regression" ID="ID_1011056459" CREATED="1482115780339" MODIFIED="1482117300786">
<font BOLD="false"/>
<node TEXT="with one variable" FOLDED="true" ID="ID_846859448" CREATED="1482115786884" MODIFIED="1482115790181">
<node TEXT="&quot;univariate&quot;" ID="ID_1145208837" CREATED="1482115791748" MODIFIED="1482115796037"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="cost" ID="ID_1342620212" CREATED="1482118697536" MODIFIED="1482118698546">
<node TEXT="\latex $J(\theta_{0}, \theta_{1})$" ID="ID_497918320" CREATED="1482118099795" MODIFIED="1482118794481">
<font BOLD="false"/>
<node TEXT="&quot;squared error function&quot;" ID="ID_754296242" CREATED="1482118338988" MODIFIED="1482118766413"/>
<node TEXT="function of parameters" ID="ID_529115142" CREATED="1482118773400" MODIFIED="1482118787529"/>
<node TEXT="minimize" ID="ID_1279055257" CREATED="1482120398749" MODIFIED="1482120404526"/>
</node>
</node>
</node>
<node TEXT="contour" ID="ID_1206432090" CREATED="1482120655299" MODIFIED="1482120659972">
<node TEXT="plots" ID="ID_1922787186" CREATED="1482120662907" MODIFIED="1482120664980"/>
<node TEXT="figures" ID="ID_1810789076" CREATED="1482120665355" MODIFIED="1482120667964"/>
</node>
<node TEXT="gradient descent" ID="ID_335924036" CREATED="1482120911950" MODIFIED="1482120917904">
<node TEXT="repeat until convergence" ID="ID_114591444" CREATED="1482120960258" MODIFIED="1482121107006">
<node TEXT="\latex $\theta_{j} := \theta_{j} - \alpha \frac{\partial}{\partial \theta_{j}}J(\theta_{0}, \theta_{1})$" ID="ID_1212535144" CREATED="1482121098179" MODIFIED="1482121301264">
<node TEXT="for j=1 and j=0" ID="ID_529925392" CREATED="1482121314860" MODIFIED="1482121321381">
<node TEXT="simultaneously" ID="ID_1639372723" CREATED="1482121436971" MODIFIED="1482121441829"/>
</node>
<node TEXT="\latex \alpha" ID="ID_975114938" CREATED="1482121387621" MODIFIED="1482121394374">
<node TEXT="&quot;learning rate&quot;" ID="ID_132038234" CREATED="1482121396085" MODIFIED="1482121402830"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="week 2" POSITION="right" ID="ID_187569800" CREATED="1483219036427" MODIFIED="1483220008939">
<edge COLOR="#808080"/>
<node TEXT="multivariate linear regression" ID="ID_976167598" CREATED="1483223607082" MODIFIED="1483223613532">
<node TEXT="hypothesis function h" ID="ID_541013175" CREATED="1482115698291" MODIFIED="1483219471606">
<node TEXT="\latex $h_{\theta} = \theta_{0} x_{0} + \theta_{1}x_{1} + \theta_{2}x_{2} + \dots + \theta_{n}x_{n}  $" ID="ID_273993105" CREATED="1483218609608" MODIFIED="1483218919355">
<node TEXT="\latex $x_{0} = 1$" ID="ID_1348276703" CREATED="1483218826997" MODIFIED="1483218843240">
<node TEXT="convenience feature" ID="ID_195742126" CREATED="1483218778484" MODIFIED="1483218942324"/>
</node>
</node>
<node TEXT="broken into vectors" ID="ID_1004274953" CREATED="1483219122893" MODIFIED="1483219134256">
<node TEXT="\latex $\theta^{T} x$" ID="ID_747957472" CREATED="1483219135541" MODIFIED="1483219515380">
<font BOLD="false"/>
<node TEXT="\latex $\theta^{T} = [ \theta_{0}  \quad  \theta_{1}  \dots  \theta_{n}  ]$" ID="ID_17804240" CREATED="1483219174623" MODIFIED="1483219328576"/>
<node TEXT="\latex $x=\begin{bmatrix}&#xa;         x_{0} \\&#xa;         x_{1} \\&#xa;         \dots\\&#xa;        x_{n}&#xa;        \end{bmatrix}$" ID="ID_77690464" CREATED="1483219339609" MODIFIED="1483219455005"/>
</node>
</node>
<node TEXT="for multiple features" ID="ID_1125508978" CREATED="1483218584624" MODIFIED="1483218589251">
<node TEXT="n is number of features" ID="ID_1849206833" CREATED="1483218722531" MODIFIED="1483218728094"/>
<node TEXT="multivariate linear regression" ID="ID_687993820" CREATED="1483219559540" MODIFIED="1483219569471">
<node TEXT="Linear regression with multiple variables" ID="ID_1996837950" CREATED="1483219605070" MODIFIED="1483219605070"/>
</node>
</node>
</node>
<node TEXT="notation" ID="ID_1332246675" CREATED="1483219623246" MODIFIED="1483219624984">
<node TEXT="n" ID="ID_1611051636" CREATED="1483219626246" MODIFIED="1483219627047">
<node TEXT="number of features" ID="ID_63033271" CREATED="1483219642838" MODIFIED="1483219646256">
<node TEXT="\latex $n =  | x^{(i)} | $" ID="ID_140069128" CREATED="1483219653742" MODIFIED="1483219853069"/>
</node>
</node>
<node TEXT="m" ID="ID_86251564" CREATED="1483219627702" MODIFIED="1483219628431">
<node TEXT="number of training examples" ID="ID_807866555" CREATED="1483219629758" MODIFIED="1483219640849"/>
</node>
<node TEXT="\latex $ x^{(i)} $" ID="ID_416663848" CREATED="1483219862921" MODIFIED="1483219878932">
<node TEXT="the column vector of all the feature inputs of the ith training example" ID="ID_166644592" CREATED="1483219888479" MODIFIED="1483219888479"/>
</node>
<node TEXT="\latex $ x^{(i)}_{j} $" ID="ID_89880283" CREATED="1483219889978" MODIFIED="1483219925403">
<node TEXT="value of feature j in the ith training example" ID="ID_993444381" CREATED="1483219904958" MODIFIED="1483219904958"/>
</node>
</node>
<node TEXT="\latex $ X = \begin{bmatrix}&#xa;         x^{(1)}_{0} \quad  x^{(1)}_{1}\\&#xa;         x^{(2)}_{0} \quad  x^{(2)}_{1}\\&#xa;         x^{(3)}_{0} \quad  x^{(3)}_{1}\\ &#xa;        \end{bmatrix}, \theta = \begin{bmatrix} \theta_{0} \\ \theta_{1}\end{bmatrix} $" ID="ID_909986457" CREATED="1483219982076" MODIFIED="1483220222671">
<node TEXT="\latex $ h_{\theta}(X) = X\theta $" ID="ID_1277231513" CREATED="1483220238343" MODIFIED="1483220271010"/>
</node>
<node TEXT="feature scaling" ID="ID_54494077" CREATED="1483220727695" MODIFIED="1483220730944">
<node TEXT="Feature scaling involves dividing the input values by the range (i.e. the maximum value minus the minimum value) of the input variable, resulting in a new range of just 1." ID="ID_1667569678" CREATED="1483221351029" MODIFIED="1483221351029"/>
</node>
<node TEXT="mean normalization" ID="ID_278757838" CREATED="1483221274814" MODIFIED="1483221279745">
<node TEXT="Mean normalization involves subtracting the average value for an input variable from the values for that input variable resulting in a new average value for the input variable of just zero." ID="ID_1334181604" CREATED="1483221367148" MODIFIED="1483221367148"/>
<node TEXT="\latex $ x := \frac{ x_{i} - \mu_{i}  }{s_{i}} $" ID="ID_90523848" CREATED="1483221391785" MODIFIED="1483221455300">
<node TEXT="\latex $ \mu_{i} $" ID="ID_269339479" CREATED="1483221479460" MODIFIED="1483221682870">
<node TEXT="is the average of all the values for feature (i)" ID="ID_1098506058" CREATED="1483221685463" MODIFIED="1483221685463"/>
</node>
<node TEXT="\latex $s_{i}$" ID="ID_1454639966" CREATED="1483221532507" MODIFIED="1483221546485">
<node TEXT="either" ID="ID_1253549662" CREATED="1483221554435" MODIFIED="1483221557597">
<node TEXT="range of values (max - min)" ID="ID_1274052489" CREATED="1483221565677" MODIFIED="1483221565677">
<node TEXT="The quizzes in this course use range" ID="ID_1336528264" CREATED="1483221604446" MODIFIED="1483221604446"/>
</node>
<node TEXT="the standard deviation" ID="ID_1655009049" CREATED="1483221585349" MODIFIED="1483221585349">
<node TEXT="the programming exercises use standard deviation." ID="ID_190831806" CREATED="1483221612542" MODIFIED="1483221612542"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="learning rate" ID="ID_1195320095" CREATED="1483221756726" MODIFIED="1483221759672">
<node TEXT="\latex $\theta_{j} := \theta_{j} - \alpha \frac{\partial}{\partial \theta_{j}}J(\theta)$" ID="ID_693352275" CREATED="1483221761937" MODIFIED="1483221776696"/>
<node TEXT="&#x3b1;" ID="ID_1927989684" CREATED="1483222323119" MODIFIED="1483222358892">
<node TEXT="If &#x3b1; is too small: slow convergence." ID="ID_464569691" CREATED="1483222340387" MODIFIED="1483222340387"/>
<node TEXT="If &#x3b1; is too large: may not decrease on every iteration and thus may not converge." ID="ID_550030430" CREATED="1483222347483" MODIFIED="1483222381881">
<node TEXT="\latex $ J(\theta)$" ID="ID_1865225100" CREATED="1483221901888" MODIFIED="1483221915057">
<node TEXT="should decrease after every iteration" ID="ID_559613136" CREATED="1483221915984" MODIFIED="1483221923794"/>
<node TEXT="if it is going up maybe use smaller learning rate" ID="ID_1775732063" CREATED="1483222070002" MODIFIED="1483222104405"/>
</node>
</node>
</node>
<node TEXT="tips" ID="ID_1424601992" CREATED="1483221827279" MODIFIED="1483221828841">
<node TEXT="make sure gradient descent is working" ID="ID_1215191478" CREATED="1483221817935" MODIFIED="1483221826336">
<node TEXT="automatic convergence test" ID="ID_557589222" CREATED="1483221968185" MODIFIED="1483221973378">
<node TEXT="declare convergence if" ID="ID_280556330" CREATED="1483221975346" MODIFIED="1483221982115">
<node TEXT="\latex $ J(\theta)$" ID="ID_1659456994" CREATED="1483221901888" MODIFIED="1483221915057"/>
<node TEXT="decreases by less than some small value in one iteration" ID="ID_1252037918" CREATED="1483221992634" MODIFIED="1483222048676"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Polynomial Regression" ID="ID_284122673" CREATED="1483223642815" MODIFIED="1483223642815">
<node TEXT="Our hypothesis function need not be linear (a straight line) if that does not fit the data well." ID="ID_1770922667" CREATED="1483223652173" MODIFIED="1483223652173"/>
<node TEXT="We can change the behavior or curve of our hypothesis function by making it a quadratic, cubic or square root function (or any other form)." ID="ID_516469938" CREATED="1483223652173" MODIFIED="1483223652173"/>
<node TEXT="For example, if our hypothesis function is h&#x3b8;(x)=&#x3b8;0+&#x3b8;1x1 then we can create additional features based on x1, to get the quadratic function h&#x3b8;(x)=&#x3b8;0+&#x3b8;1x1+&#x3b8;2x21 or the cubic function h&#x3b8;(x)=&#x3b8;0+&#x3b8;1x1+&#x3b8;2x21+&#x3b8;3x31" ID="ID_1472543943" CREATED="1483223652173" MODIFIED="1483223652173"/>
<node TEXT="In the cubic version, we have created new features x2 and x3 where x2=x21 and x3=x31." ID="ID_17491884" CREATED="1483223652175" MODIFIED="1483223652175"/>
<node TEXT="To make it a square root function, we could do: h&#x3b8;(x)=&#x3b8;0+&#x3b8;1x1+&#x3b8;2x1&#x203e;&#x203e;&#x221a;" ID="ID_788052207" CREATED="1483223652176" MODIFIED="1483223652176"/>
<node TEXT="eg. if x1 has range 1 - 1000 then range of x21 becomes 1 - 1000000 and that of x31 becomes 1 - 1000000000" ID="ID_1778544291" CREATED="1483223652179" MODIFIED="1483223652179"/>
<node TEXT="One important thing to keep in mind is, if you choose your features this way then feature scaling becomes very important." ID="ID_918578421" CREATED="1483223652177" MODIFIED="1483223652177"/>
</node>
</node>
<node TEXT="computing parameters analytically" ID="ID_481421025" CREATED="1483223704388" MODIFIED="1483223712118">
<node TEXT="normal equation" ID="ID_1633201165" CREATED="1483223716540" MODIFIED="1483223720246"/>
</node>
</node>
<node TEXT="octave" POSITION="left" ID="ID_1679705846" CREATED="1481732546338" MODIFIED="1481732549458">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="learning types" FOLDED="true" POSITION="left" ID="ID_432034625" CREATED="1481132629018" MODIFIED="1481132635884">
<edge COLOR="#ff0000"/>
<node TEXT="supervised" FOLDED="true" ID="ID_573030108" CREATED="1481132636746" MODIFIED="1481132639024">
<node TEXT="problem types" FOLDED="true" ID="ID_338565627" CREATED="1481132644334" MODIFIED="1481132652077">
<node TEXT="Regression" FOLDED="true" ID="ID_1385684060" CREATED="1481132653094" MODIFIED="1481132655605">
<node TEXT="continuous valued output" ID="ID_100672825" CREATED="1481132670171" MODIFIED="1481132675537"/>
</node>
<node TEXT="Classification" FOLDED="true" ID="ID_1590701956" CREATED="1481132656039" MODIFIED="1481132658910">
<node TEXT="discrete valued output" ID="ID_641602288" CREATED="1481132660255" MODIFIED="1481132668398"/>
</node>
</node>
<node TEXT="labelled data" ID="ID_1494298125" CREATED="1481134584603" MODIFIED="1481134591830"/>
</node>
<node TEXT="unsupervised" FOLDED="true" ID="ID_1004431859" CREATED="1481132639454" MODIFIED="1481132643132">
<node TEXT="no labelled data" ID="ID_1528816934" CREATED="1481134600936" MODIFIED="1481134606030"/>
<node TEXT="clustering algorithm" ID="ID_1053504906" CREATED="1481134626093" MODIFIED="1481134630724"/>
<node TEXT="Cocktail Party Problem" ID="ID_1145287043" CREATED="1481134839637" MODIFIED="1481134846204"/>
</node>
</node>
</node>
</map>
